package com.bcoe.bricarbon.vo;

import java.io.Serializable;

/**
 * <p>Title: ReqNewChain</p>
 * <p>Description: </p>
 * @author he_jiebing@jiuyv.com
 @date   2021年4月23日 下午3:50:09
 */

public class ReqNewCarbon implements Serializable{

    private static final long serialVersionUID = -6094498793318107274L;
    private String userId;
    private String desc;
    //private String role;
    private String projectName;
    private String projectId;
    private String issuer;
    private Integer creditAmount;
    private String carbonYearFrom;
    private String carbonYearTo;
    private Integer offerPrice;
    private String coBenefit;
    private String sdgNumber;
    private String registryLink;
    private String registryAccount;
    private String registryNo;
    private String imageProject;
    private String participaterId;

    //GET & SET Variabel


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectId() { return projectId; }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getIssuer() { return issuer; }
    public void setIssuer(String issuer) { this.issuer = issuer;}

    public Integer getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCarbonYearFrom() { return carbonYearFrom; }

    public void setCarbonYearFrom(String carbonYearFrom) {
        this.carbonYearFrom = carbonYearFrom;
    }

    public String getCarbonYearTo() { return carbonYearTo; }

    public void setCarbonYearTo(String carbonYearTo) {
        this.carbonYearTo = carbonYearTo;
    }


    public Integer getOfferPrice() { return offerPrice; }

    public void setOfferPrice(Integer offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getCoBenefit() { return coBenefit; }

    public void setCoBenefit(String coBenefit) {
        this.coBenefit = coBenefit;
    }

    public String getSdgNumber() { return sdgNumber; }

    public void setSdgNumber(String sdgNumber) {
        this.sdgNumber = sdgNumber;
    }

    public String getRegistryLink() { return registryLink; }

    public void setRegistryLink(String registryLink) {
        this.registryLink = registryLink;
    }

    public String getRegistryAccount() { return registryAccount; }

    public void setRegistryAccount(String registryAccount) {
        this.registryAccount = registryAccount;
    }

    public String getRegistryNo() { return registryNo; }

    public void setRegistryNo(String registryNo) {
        this.registryNo = registryNo;
    }

    public String getImageProject() { return imageProject; }

    public void setImageProject(String imageProject) {
        this.imageProject = imageProject;
    }

    public String getParticipaterId() { return participaterId; }

    public void setParticipaterId(String participaterId) {
        this.participaterId = participaterId; }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    //public String getRole() {return role;}

    //public void setRole(String role) {this.role = role;}





}
