package com.bcoe.bricarbon.vo;

import java.io.Serializable;

/**
 * <p>Title: ReqPay</p>
 * <p>Description: </p>
 * @author he_jiebing@jiuyv.com
   @date   2021年4月25日 下午3:01:10
 */

public class ReqSentCarbon implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3028096024011452390L;

	/**
	 * 链ID
	 */
	private String carbonId;
	/**
	 * 支付总金额
	 */
	private Long totalAmount;
	public String getCarbonId() {
		return carbonId;
	}
	public void setCarbonId(String carbonId) {
		this.carbonId = carbonId;
	}
	public Long getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	

}
