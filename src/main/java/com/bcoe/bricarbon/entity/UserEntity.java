package com.bcoe.bricarbon.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author he_jiebing@jiuyv.com
 * @date 2021-04-20 15:58:29
 */
@TableName("user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.INPUT)
	private String id;
	private String username;
	private String encryptedPassword;
	private String idCard;
	private String firstName;
	private String lastName;
	private String phone;
	private Date insertedAt;
	private Date updatedAt;
	private String participaterId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	//Penambahan code_stack untuk flagging
	//public String getCode_stack() { return code_stack; }
	//public void setCode_stack(String code_stack) { this.code_stack = code_stack;}

	public String getIdCard() { return idCard; }
	public void setIdCard(String idCard) { this.idCard = idCard; }



	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	public String getFirstName() {return firstName; }
	public void setFirstName (String firstName) { this.firstName = firstName;}
	public String getLastName () { return lastName; }
	public void setLastName (String lastName) { this.lastName = lastName; }
	public String getPhone() { return phone; }
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getInsertedAt() {
		return insertedAt;
	}
	public void setInsertedAt(Date insertedAt) {
		this.insertedAt = insertedAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getParticipaterId() {
		return participaterId;
	}
	public void setParticipaterId(String participaterId) {
		this.participaterId = participaterId;
	}
	

}
