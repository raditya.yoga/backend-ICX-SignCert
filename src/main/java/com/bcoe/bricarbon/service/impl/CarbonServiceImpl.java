package com.bcoe.bricarbon.service.impl;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import cn.hutool.core.lang.UUID;
import com.bcoe.bricarbon.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bcoe.bricarbon.bo.ContractDeployReqBO;
import com.bcoe.bricarbon.bo.TransDataRespBO;
import com.bcoe.bricarbon.bo.TransHandleReqBO;
import com.bcoe.bricarbon.common.GlobalConstant;
import com.bcoe.bricarbon.common.R;
import com.bcoe.bricarbon.dao.CarbonDao;
import com.bcoe.bricarbon.dao.ContractDao;
import com.bcoe.bricarbon.dao.ContractTemplateDao;
import com.bcoe.bricarbon.dao.EvidenceDao;
import com.bcoe.bricarbon.dao.ParticipaterDao;
import com.bcoe.bricarbon.entity.CarbonEntity;
import com.bcoe.bricarbon.entity.ContractEntity;
import com.bcoe.bricarbon.entity.ContractTemplateEntity;
import com.bcoe.bricarbon.entity.EvidenceEntity;
import com.bcoe.bricarbon.entity.ParticipaterEntity;
import com.bcoe.bricarbon.enums.SignFlagEnum;
import com.bcoe.bricarbon.enums.StatusEnum;
import com.bcoe.bricarbon.enums.TypeEnum;
import com.bcoe.bricarbon.service.CarbonService;
import com.bcoe.bricarbon.util.DateUtils;
import com.bcoe.bricarbon.util.HttpUtils;

import com.webank.webase.app.sdk.client.AppClient;
import com.webank.webase.app.sdk.config.HttpConfig;
import com.webank.webase.app.sdk.dto.req.ReqContractAddressSave;
import com.webank.webase.app.sdk.dto.req.ReqContractSourceSave;
import com.webank.webase.app.sdk.dto.req.ReqContractSourceSave.ContractSource;

/**
 *
 * <p>Title: CarbonServiceImpl</p>
 * <p>Description: </p>
 * @author he_jiebing@jiuyv.com
 @date   2021年4月25日 下午5:58:09
 */
@Service("carbonService")
public class CarbonServiceImpl extends ServiceImpl<CarbonDao, CarbonEntity> implements CarbonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CarbonServiceImpl.class);

    @Autowired
    private CarbonDao carbonDao;


    @Autowired
    private ParticipaterDao participaterDao;

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private EvidenceDao evidenceDao;

    @Autowired
    private ContractTemplateDao contractTemplateDao;

    @Value("${webase-front.contract.deploy.url}")
    private String webaseFrontContractDeployUrl;

    @Value("${webase-front.trans.handle.url}")
    private String webaseFrontTransHandleUrl;

    //@Value("${erc20.carbon.user.signUserId}")
   // private String erc20SupplySignUserId;

    @Value("${erc20.contract.address}")
    private String erc20ContractAddress;

    @Value("${erc20.contract.name}")
    private String erc20ContractName;

    @Value("${webase.node.mgr.url}")
    private String url;

    @Value("${webase.node.mgr.appKey}")
    private String appKey;

    @Value("${webase.node.mgr.appSecret}")
    private String appSecret;

    @Value("${webase.node.mgr.isTransferEncrypt}")
    private Boolean isTransferEncrypt;


    private static final String ACCOUNT = "admin";

    private static final String CONTRACT_VERSION = "1.0.0";

    private static final String CONTRACT_NAME_EVIDENCE_FACTORY = "EvidenceFactory";

    private static final String CONTRACT_NAME_EVIDENCE = "Carbon";

    public static final String ROLE_VERIF = "Verra Verificator";
    public static final String VERIFIER_ADDRESS = "0x0d625de0448870b3e99e4d48400dddb012b6d8bd";


    public static final String VERIF_ID = "78dfab2edfaf405b93c6484f9debec41";


    @Transactional(rollbackFor=Exception.class)
    @Override
    public R newCarbon(ReqNewCarbon reqNewCarbon) {
        // To build a chain, each supplier must be signed
        List<String> participaterAddrs = new ArrayList<String>();
        // signed suppliers
        List<String> signedParticipaterIds = new ArrayList<String>();
        List<String> participaterIds = new ArrayList<String>();
        Date date = Date.from(Instant.now());
        ParticipaterEntity participaterEntity = participaterDao.queryByParticipaterId(reqNewCarbon.getParticipaterId());
        // 1. Landing the carbon table
        CarbonEntity entity = new CarbonEntity();
        String id = UUID.fastUUID().toString().replaceAll("-", "");
        // Ngambil id dari participaterDao
        entity.setId(id);
        entity.setUserId(participaterEntity.getUserId());
        entity.setCarbonDesc(reqNewCarbon.getDesc());
        entity.setInsertedAt(date);
        entity.setUpdatedAt(date);
        entity.setSignStatus(StatusEnum.DRAFT.getStatus());
        entity.setProjectName(reqNewCarbon.getProjectName());
        entity.setProjectId(reqNewCarbon.getProjectId());
        entity.setIssuer(reqNewCarbon.getIssuer());
        entity.setCreditInitial(reqNewCarbon.getCreditAmount());
        entity.setCreditAmount(reqNewCarbon.getCreditAmount());
        entity.setCarbonYearFrom(reqNewCarbon.getCarbonYearFrom());
        entity.setCarbonYearTo(reqNewCarbon.getCarbonYearTo());
        entity.setOfferPrice(reqNewCarbon.getOfferPrice());
        entity.setCoBenefit(reqNewCarbon.getCoBenefit());
        entity.setSdgNumber(reqNewCarbon.getSdgNumber());
        entity.setRegistryLink(reqNewCarbon.getRegistryLink());
        entity.setRegistryAccount(reqNewCarbon.getRegistryAccount());
        entity.setRegistryNo(reqNewCarbon.getRegistryNo());
        entity.setImageProject(reqNewCarbon.getImageProject());
        String participaterId = reqNewCarbon.getParticipaterId();
        entity.setParticipaterId(participaterId);
        entity.setRole(GlobalConstant.ROLE_OWNER);
        entity.setIsSigned(SignFlagEnum.UNSIGN.getSignFlag());

        signedParticipaterIds.add(participaterId);
        carbonDao.insert(entity);

        // 3.1 Call the webase-front deployment contract method
        // Get the addresses on the chain of all participants
        participaterIds.add(participaterId);
        participaterIds.add(VERIF_ID);

        participaterAddrs.add(participaterEntity.getUserAddress());
        participaterAddrs.add(VERIFIER_ADDRESS);

        ParticipaterEntity eviMapDb = participaterDao.selectById(reqNewCarbon.getParticipaterId());
        Map<String,String> evidenceValMap = new HashMap<>();
        evidenceValMap.put(eviMapDb.getUserAddress(), entity.getRole());
        evidenceValMap.put(VERIFIER_ADDRESS, ROLE_VERIF);

        ContractDeployReqBO contractDeployReqBO = new ContractDeployReqBO();
        String contractName = TypeEnum.EVIDENCE.getType()+"_"+DateUtils.dateTimeNow();
        ContractTemplateEntity template = contractTemplateDao.queryByTemplate(GlobalConstant.EVIDENCE_FACTORY_CONTRACT_TEMPLATE);
        JSONArray parseArray = JSONUtil.parseArray(template.getContractAbi());
        List<Object> abiList = JSONUtil.toList(parseArray, Object.class);
        contractDeployReqBO.setGroupId(1);
        contractDeployReqBO.setAbiInfo(abiList);
        contractDeployReqBO.setBytecodeBin(template.getContractBin());
        contractDeployReqBO.setContractName(contractName);
        contractDeployReqBO.setContractSource(template.getContractBase64());
        contractDeployReqBO.setVersion(CONTRACT_VERSION);
        List list = new ArrayList<>();
        list.add(participaterAddrs);
        contractDeployReqBO.setFuncParam(list);
        //contractDeployReqBO.setUser(poarticipaterEntity.getUserAddress());
        contractDeployReqBO.setSignUserId(participaterEntity.getSignUserId());
        LOGGER.info("Call the webase-front interface, url>>{}, request parameters:>>{}",webaseFrontContractDeployUrl,JSONUtil.toJsonStr(contractDeployReqBO));
        String response = HttpUtils.httpPostByJson(webaseFrontContractDeployUrl, JSONUtil.toJsonStr(contractDeployReqBO));
        LOGGER.info("Call the webase-front interface and respond with the result:>>{}",response);

        // TODO calls webase-sdk synchronous binding
        AppClient appClient = getAppClient();
        callWebaseSdkContractSourceSave(appClient,contractDeployReqBO);

        callWebaseSdkContractAddressSave(appClient,contractDeployReqBO,response,participaterEntity.getNameOnWebase());

        // 3.2 landing contract table
        ContractEntity contractEntity = new ContractEntity();
        contractEntity.setAddr(response);
        contractEntity.setCarbonId(entity.getId());
        contractEntity.setContractName(contractName);
        contractEntity.setContractDescribe(TypeEnum.EVIDENCE.getType());
        contractEntity.setInsertedAt(date);
        contractEntity.setOwnerDid(participaterEntity.getDid());
        contractEntity.setType(TypeEnum.EVIDENCE.getType());
        contractEntity.setUpdatedAt(date);
        contractDao.insert(contractEntity);
        // 4.1 Call the newEvidence method
        TransHandleReqBO transHandleReqBO = new TransHandleReqBO();
        transHandleReqBO.setContractAbi(abiList);
        // deployed contract address
        transHandleReqBO.setContractAddress(response);
        transHandleReqBO.setContractName(contractName);
        transHandleReqBO.setFuncName("newEvidence");
        List params = new ArrayList();
        params.add(JSONUtil.toJsonStr(evidenceValMap));
        transHandleReqBO.setFuncParam(params);
        transHandleReqBO.setGroupId(1);
        transHandleReqBO.setUseCns(false);
        //transHandleReqBO.setUser(poarticipaterEntity.getUserAddress());
        transHandleReqBO.setSignUserId(participaterEntity.getSignUserId());
        LOGGER.info("Call the webase-front interface, url>>{}, request parameters:>>{}",webaseFrontTransHandleUrl,JSONUtil.toJsonStr(transHandleReqBO));
        String resp = HttpUtils.httpPostByJson(webaseFrontTransHandleUrl, JSONUtil.toJsonStr(transHandleReqBO));
        LOGGER.info("Call the webase-front interface and respond with the result:>>{}",resp);
        TransDataRespBO resBO = JSONUtil.toBean(resp, TransDataRespBO.class);
        // 4.2 Landing evidence table
        EvidenceEntity evidenceEntity = new EvidenceEntity();
        evidenceEntity.setContractId(contractEntity.getId());
        evidenceEntity.setEvidenceDescribe(TypeEnum.EVIDENCE.getType());
        evidenceEntity.setInsertedAt(date);
        evidenceEntity.setEvidenceKey(resBO.getLogs().get(0).getAddress());
        evidenceEntity.setOwners(JSONUtil.toJsonStr(participaterIds));
        evidenceEntity.setSigners(JSONUtil.toJsonStr(signedParticipaterIds));
        List<String> txIds = new ArrayList<String>();
        txIds.add(resBO.getTransactionHash());
        evidenceEntity.setTxId(JSONUtil.toJsonStr(txIds));
        evidenceEntity.setUpdatedAt(date);
        evidenceEntity.setEvidenceValue(JSONUtil.toJsonStr(evidenceValMap));
        evidenceDao.insert(evidenceEntity);
        return R.ok();
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public R sign(ReqSign reqSign) {
        Date date = Date.from(Instant.now());
        // According to userId
        String participaterId = reqSign.getParticipaterId();
        // 1. Check already signed, do not need to re-sign
        ParticipaterEntity dbParticipaterEntity = participaterDao.selectById(participaterId);
        ContractEntity dbContract = contractDao.queryByCarbonId(reqSign.getCarbonId());
        EvidenceEntity dbEvidence = evidenceDao.queryByContractId(dbContract.getId());
        String owners = dbEvidence.getOwners();
        List<String> ownerparticipaterIds = com.alibaba.fastjson.JSONArray.parseArray(owners, String.class);
        String signers = dbEvidence.getSigners();
        List<String> signedparticipaterIds = com.alibaba.fastjson.JSONArray.parseArray(signers, String.class);
        if(ownerparticipaterIds.contains(participaterId)&&signedparticipaterIds.contains(participaterId)){
            return R.error("The user has signed, no need to re-sign");
        }
        // 2. Call the addSignatures method of the contract
        TransHandleReqBO transHandleReqBO = new TransHandleReqBO();
        ContractTemplateEntity template = contractTemplateDao.queryByTemplate(GlobalConstant.EVIDENCE_FACTORY_CONTRACT_TEMPLATE);
        JSONArray parseArray = JSONUtil.parseArray(template.getContractAbi());
        List<Object> abiList = JSONUtil.toList(parseArray, Object.class);
        transHandleReqBO.setContractAbi(abiList);
        // The deployed contract address
        transHandleReqBO.setContractAddress(dbContract.getAddr());
        transHandleReqBO.setContractName(dbContract.getContractName());
        transHandleReqBO.setFuncName("addSignatures");
        List params = new ArrayList();
        params.add(dbEvidence.getEvidenceKey());
        transHandleReqBO.setFuncParam(params);
        transHandleReqBO.setGroupId(1);
        transHandleReqBO.setUseCns(false);
        //transHandleReqBO.setUser(dbParticipaterEntity.getUserAddress());
        transHandleReqBO.setSignUserId(dbParticipaterEntity.getSignUserId());
        LOGGER.info("Call the webase-front interface, url>>{}, request parameters: >>{}",webaseFrontTransHandleUrl,JSONUtil.toJsonStr(transHandleReqBO));
        String resp = HttpUtils.httpPostByJson(webaseFrontTransHandleUrl, JSONUtil.toJsonStr(transHandleReqBO));
        LOGGER.info("Call the webase-front interface and respond with result: >>{}",resp);
        TransDataRespBO resBO = JSONUtil.toBean(resp, TransDataRespBO.class);
        // 3. Update the is_signed field of the item table
        CarbonEntity dbCheck = carbonDao.queryById(reqSign.getCarbonId());
        CarbonEntity query = new CarbonEntity();
        query.setId(reqSign.getCarbonId());
        query.setParticipaterId(dbCheck.getParticipaterId());
        CarbonEntity dbEntity = carbonDao.queryInfo(query);
        CarbonEntity updateEntity = new CarbonEntity();
        updateEntity.setId(dbEntity.getId());
        updateEntity.setIsSigned(SignFlagEnum.SIGNED.getSignFlag());
        updateEntity.setSignStatus(StatusEnum.CONFIRMED.getStatus());
        updateEntity.setUpdatedAt(date);
        carbonDao.updateById(updateEntity);
        // 4. Update the tx_id and signers fields of the evidence table
        EvidenceEntity dbEvidenceEntity = new EvidenceEntity();
        dbEvidenceEntity.setId(dbEvidence.getId());
        JSONArray txIdArray = JSONUtil.parseArray(dbEvidence.getTxId());
        List<String> txIds = JSONUtil.toList(txIdArray, String.class);
        txIds.add(resBO.getTransactionHash());
        dbEvidenceEntity.setTxId(JSONUtil.toJsonStr(txIds));
        JSONArray signerArray = JSONUtil.parseArray(dbEvidence.getSigners());
        List<String> signerList = JSONUtil.toList(signerArray, String.class);
        signerList.add(participaterId);
        dbEvidenceEntity.setSigners(JSONUtil.toJsonStr(signerList));
        evidenceDao.updateById(dbEvidenceEntity);
        // 5. Update the sign_status field of the carbon main table
        // TODO calls the on-chain contract method getEvidence method to compare whether the owners field and the signers field of the evidence table are equal
        EvidenceEntity evidenceEntity = evidenceDao.selectById(dbEvidence.getId());
        String owners2 = evidenceEntity.getOwners();
        List<String> ownerparticipaterIds2 = com.alibaba.fastjson.JSONArray.parseArray(owners2, String.class);
        String signers2 = evidenceEntity.getSigners();
        List<String> signedparticipaterIds2 = com.alibaba.fastjson.JSONArray.parseArray(signers2, String.class);
        boolean flag = ownerparticipaterIds2.stream().sorted().collect(Collectors.joining()).equals(signedparticipaterIds2.stream().sorted().collect(Collectors.joining()));
        CarbonEntity updateCarbonEntity = new CarbonEntity();
        if(flag){
            updateCarbonEntity.setSignStatus(StatusEnum.CONFIRMED.getStatus());
            updateCarbonEntity.setId(reqSign.getCarbonId());
            carbonDao.updateById(updateCarbonEntity);
        }
        ResSign resSign = new ResSign();
        resSign.setTxHash(resBO.getTransactionHash());
        return R.ok(resSign);
    }

    private void callWebaseSdkContractSourceSave(AppClient appClient,ContractDeployReqBO contractDeployReqBO){
        ReqContractSourceSave reqContractSourceSave = new ReqContractSourceSave();
        reqContractSourceSave.setAccount(ACCOUNT);
        reqContractSourceSave.setContractVersion(CONTRACT_VERSION);

        List<ContractSource> contractList = new ArrayList<>();
        // add EvidenceFactory contract
        ContractSource evidenceFactoryContractSource = new ContractSource();
        evidenceFactoryContractSource.setContractName(CONTRACT_NAME_EVIDENCE_FACTORY);
        evidenceFactoryContractSource.setContractSource(contractDeployReqBO.getContractSource());
        evidenceFactoryContractSource.setContractAbi(JSONUtil.toJsonStr(contractDeployReqBO.getAbiInfo()));
        evidenceFactoryContractSource.setBytecodeBin(contractDeployReqBO.getBytecodeBin());
        // add Evidence contract
        ContractSource evidenceContractSource = new ContractSource();
        ContractTemplateEntity template = contractTemplateDao.queryByTemplate(GlobalConstant.EVIDENCE_CONTRACT_TEMPLATE);
        JSONArray parseArray = JSONUtil.parseArray(template.getContractAbi());
        List<Object> abiList = JSONUtil.toList(parseArray, Object.class);
        evidenceContractSource.setContractName(CONTRACT_NAME_EVIDENCE);
        evidenceContractSource.setContractSource(template.getContractBase64());
        evidenceContractSource.setContractAbi(JSONUtil.toJsonStr(abiList));
        evidenceContractSource.setBytecodeBin(template.getContractBin());



        contractList.add(evidenceContractSource);
        contractList.add(evidenceFactoryContractSource);
        reqContractSourceSave.setContractList(contractList);
        LOGGER.info("Call the WebaseSdk ContractSourceSave interface, request parameters: >>{}",JSONUtil.toJsonStr(reqContractSourceSave));

        appClient.contractSourceSave(reqContractSourceSave);
    }
    private void callWebaseSdkContractAddressSave(AppClient appClient,ContractDeployReqBO contractDeployReqBO,String contractAddr,String username){
        ReqContractAddressSave reqContractAddressSave = new ReqContractAddressSave();
        reqContractAddressSave.setGroupId(1);
        reqContractAddressSave.setContractName(CONTRACT_NAME_EVIDENCE_FACTORY);
        reqContractAddressSave.setContractPath(GlobalConstant.APP_PREIX+username+"_"+DateUtils.dateTimeNow());
        reqContractAddressSave.setContractVersion(CONTRACT_VERSION);
        reqContractAddressSave.setContractAddress(contractAddr);
        LOGGER.info("Call the WebaseSdk AddressSave interface, request parameters: >>{}",JSONUtil.toJsonStr(reqContractAddressSave));
        appClient.contractAddressSave(reqContractAddressSave);
    }

    private AppClient getAppClient(){
        HttpConfig httpConfig = new HttpConfig(30, 30, 30);
        return new AppClient(url, appKey, appSecret, isTransferEncrypt, httpConfig);
    }
}