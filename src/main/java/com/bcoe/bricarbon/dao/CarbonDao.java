package com.bcoe.bricarbon.dao;

import java.util.List;

import com.bcoe.bricarbon.entity.CarbonEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 *
 * @author he_jiebing@jiuyv.com
 * @date 2021-04-20 15:58:29
 */
@Mapper
public interface CarbonDao extends BaseMapper<CarbonEntity> {

    /**
     * 根据用户id查询链路
     * @param userId
     * @return
     */
    List<CarbonEntity> queryByUserId(@Param("userId") String userId);

    /**
     * 统计总链路数
     * @return
     */
    Integer countCarbon();

    CarbonEntity queryById(@Param("id") String id);

    /**
     * 查询供应商信息
     * @param query
     * @return
     */
    CarbonEntity queryInfo(CarbonEntity query);

}